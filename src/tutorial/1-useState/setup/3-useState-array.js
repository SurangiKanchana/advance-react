import React from 'react';
import { data } from '../../../data';

const UseStateArray = () => {
  const [people, setPeople] = React.useState(data);
  const removeItem = (_id) => {
    // ( !== ) checks whether its two operands are not equal
    /*this filter the people whose id is not equal to _id. 
    If it is equal to _id, then it will not filter thats mean it will remove
    but this doesnt remove from the database*/
    let newPeople = people.filter((person) => person.id !== _id);
    setPeople(newPeople);

  }
  return (
    <React.Fragment>
      <h2>useState array example</h2>
      {
        people.map((person) => {
          const { id, name} = person;
          return (
            <div key={id} className='item'>
              <h4>
                {name}
              </h4>
              <button type='button' onClick={() => removeItem(id)}>
                remove
              </button>
            </div>
          )
        })
      }
      <button type='button' className='btn' onClick={() => setPeople([])}>
        Clear All
      </button>
    </React.Fragment>
    );
};

export default UseStateArray;
