import React, { useState } from 'react';

const UseStateObject = () => {
  const [person, setPerson] = useState({
    name: 'surangi', 
    age:25, 
    message: 'dummy message'
  });
  const changeMessage = () => {
    setPerson({...person,message: 'Changed Message'});
  };
  return (
    <React.Fragment>
  <h2>useState object example</h2>
  <h3>{person.name}</h3>
  <h3>{person.age}</h3>
  <h4>{person.message}</h4>
  <button type='button' className='btn' onClick={changeMessage}>
    Change Message
  </button>
  </React.Fragment>
    );
};

export default UseStateObject;
