import React, { useState } from 'react';

const UseStateBasics = () => {
  //title is the value and setTitle is the function
  //must be in the component body
  const [title, setTitle] = useState('Test Title');
  const handleClick = () => { 
    if(title === 'Test Title'){
      setTitle('Changed Title'); 
    }
    else {
      setTitle('Test Title'); 
    }
  }
  return (
    <React.Fragment>
  <h2>{title}</h2>
  <button type='Button' className='btn' onClick={handleClick}>Click Here</button>
  </React.Fragment>
  );
};

export default UseStateBasics;
